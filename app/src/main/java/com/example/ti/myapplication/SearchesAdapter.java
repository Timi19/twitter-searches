package com.example.ti.myapplication;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by ti on 27/01/2017.
 */

public class SearchesAdapter extends RecyclerView.Adapter<SearchesAdapter.ViewHolder> {

    private final List<String>tags;
    private final View.OnClickListener clickListener;
    private  final View.OnLongClickListener longClickListener;

    public SearchesAdapter(List<String> tags, View.OnClickListener clickListener, View.OnLongClickListener longClickListener){
        this.tags = tags;
        this.clickListener = clickListener;
        this.longClickListener = longClickListener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private TextView textView;

        public ViewHolder(View itemView, View.OnClickListener clickListener,
                          View.OnLongClickListener longClickListener){
            super(itemView);
            textView = (TextView)itemView.findViewById(R.id.textView);

            itemView.setOnClickListener(clickListener);
            itemView.setOnLongClickListener(longClickListener);
        }

    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item,parent,false);
        return (new ViewHolder(view,clickListener,longClickListener));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.textView.setText(tags.get(position));

    }

    @Override
    public int getItemCount() {
        return tags.size();
    }




}
