package com.example.ti.myapplication;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by ti on 27/01/2017.
 */

public class ItemDivider extends RecyclerView.ItemDecoration {

    private final Drawable divider;

    public ItemDivider(Context context){

        int [] attrs = {android.R.attr.listDivider};
        divider = context.obtainStyledAttributes(attrs).getDrawable(0);

    }
    // draws list items divider onto the recycler view
    @Override
    public void onDrawOver(Canvas c , RecyclerView parent , RecyclerView.State state){
        super.onDrawOver(c,parent,state);

        //calculate left and right coordinates for all dividers

        int left = parent.getPaddingLeft();
        int right = parent.getWidth() - parent.getPaddingRight();

        // draw a line underneath each of the items except the last one

        for (int i =0; i<parent.getChildCount()-1; i++){
            View item = parent.getChildAt(i);
            //calculating bottom and top margins for the current divider
            int top = item.getBottom() + ((RecyclerView.LayoutParams)item.getLayoutParams()).bottomMargin;
            int bottom = top + divider.getIntrinsicHeight();

            // draw the divider from the calculated bounds

            divider.setBounds(left,top,right,bottom);
            divider.draw(c);
        }
    }


}
