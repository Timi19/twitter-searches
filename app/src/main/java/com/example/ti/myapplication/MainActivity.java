package com.example.ti.myapplication;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    // name of shared prefrence file that storess saved searches
    private static final String SEARCHES = "searches";

    private EditText queryEditText,tagEditText;
    private FloatingActionButton saveFloatingActionButton;
    private SharedPreferences savedSearches;
    private List<String> tags; // list of tags for saved searches
    private SearchesAdapter adapter; // for bindiing data to recycler view



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        queryEditText = ((TextInputLayout)findViewById(R.id.queryTextInputLayout)).getEditText();
        queryEditText.addTextChangedListener(textWatcher);

        tagEditText = ((TextInputLayout)findViewById(R.id.tagTextInputLayout)).getEditText();
        tagEditText.addTextChangedListener(textWatcher);

        // get shared preferences containing the users saved searches
        savedSearches = getSharedPreferences(SEARCHES,MODE_PRIVATE);

        // store the saved tags in an arrayList the sort them
        tags = new ArrayList<>(savedSearches.getAll().keySet());
        Collections.sort(tags, String.CASE_INSENSITIVE_ORDER);

        //get refrence to the recycler view and configure it
        RecyclerView recyclerView = (RecyclerView)findViewById(R.id.recyclerView);

        // use linear layout manager to display a vertical list
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        // use a RecyclerView.adapter to bind data to the RecyclerView

        adapter = new SearchesAdapter(tags, onItemClicked, onItemLongClicked);
        recyclerView.setAdapter(adapter);

        // use a custom decorator to draw a line underneath each list item in the recyclerview
        recyclerView.addItemDecoration(new ItemDivider(this));

         saveFloatingActionButton =  (FloatingActionButton)findViewById(R.id.fab);
        saveFloatingActionButton.setOnClickListener(saveButtonListener);
        updateSaveFab(); // hides the floating action button when edittext is empty
    }
    // hide or show FAB based on edit text content
    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }
         // show/hide Fab when user changes input
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            updateSaveFab();
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    //shows or hides FAB
    private void updateSaveFab(){
        // checking to see if theres input in both edittexts
        if(queryEditText.getText().toString().isEmpty()|| tagEditText.getText().toString().isEmpty()){
            saveFloatingActionButton.hide();
        }
        else {
            saveFloatingActionButton.show();
        }
    }

    private final View.OnClickListener saveButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String query = queryEditText.getText().toString();
            String tag = tagEditText.getText().toString();

            if (!query.isEmpty() && !tag.isEmpty()) {
                ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(v.getWindowToken(), 0);
                addTaggedSearch(tag, query); //add or update the search

                queryEditText.setText("");
                tagEditText.setText("");
                queryEditText.requestFocus();
            }
        }
    };
    private void addTaggedSearch(String tag , String query){
        SharedPreferences.Editor editor = savedSearches.edit();
        editor.putString(tag,query);
        editor.apply();

        if(!tags.contains(tag)){

            tags.add(tag); // add tag to list if it is new
            Collections.sort(tags,String.CASE_INSENSITIVE_ORDER);
            adapter.notifyDataSetChanged();

        }
    }
    //itemclicklistener to launch webbrowser

    private final View.OnClickListener onItemClicked = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            //get query string and Create A Url representing the search
            String tag = ((TextView)v).getText().toString();
            String urlString = getString(R.string.search_URL) + Uri.encode(savedSearches.getString(tag,""), "UTF-8");

            // activate intent  to launch web browser
            Intent webIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(urlString));
            startActivity(webIntent);
        }
    };


    //long item click listener to allow users share edit and delete a search

    private final View.OnLongClickListener onItemLongClicked = new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View v) {
            final String tag = ((TextView)v).getText().toString(); //
            // to get the tag clicked on

            //create an alert dialog
            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            // set title
            builder.setTitle(getString(R.string.share_edit_delete_title, tag));
            // to set the list of items to display and an event handler

            builder.setItems(R.array.dialog_items, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch(which){
                        case 0 : //share
                            shareSearch(tag);
                            break;
                        case 1 : //edit
                            tagEditText.setText(tag);
                            queryEditText.setText(savedSearches.getString(tag,""));
                            break;
                        case 2:
                            deleteSearch(tag);
                            break;
                    }
                }
            });
            builder.setNegativeButton(getString(R.string.cancel), null);
            builder.create().show();


            return true;
        }
    };

    // allow user to chose an app for the sharing of a saved search

    private void shareSearch(String tag){
        //create url representing the search

        String urlString = getString(R.string.search_URL)+
                Uri.encode(savedSearches.getString(tag,""),"UTF-8");

        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.share_subject));
        intent.putExtra(Intent.EXTRA_TEXT ,getString(R.string.share_message, urlString));
        intent.setType("text/plain");
         // display apps that can share plain text
        startActivity(Intent.createChooser(intent,getString(R.string.share_search)));
    }

    private void deleteSearch(final String tag){

        AlertDialog.Builder confirmDialog = new AlertDialog.Builder(MainActivity.this);
        confirmDialog.setTitle(getString(R.string.confirm_message, tag));

        confirmDialog.setNegativeButton(getString(R.string.cancel),null);

        confirmDialog.setPositiveButton(getString(R.string.delete), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                tags.remove(tag); // remove from tags

                // get preferences editor to remove saved searches

                SharedPreferences.Editor editor =  savedSearches.edit();
                editor.remove(tag);
                editor.apply();
                adapter.notifyDataSetChanged();
            }
        });
        confirmDialog.create().show();
    }
}
